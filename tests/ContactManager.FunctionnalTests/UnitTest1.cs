using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Xunit;
using FluentAssertions;
using Avico;
using Avico.DTO;
using AspNetCore.Http.Extensions;
using System.Collections.Generic;

namespace ContactManager.FunctionnalTests
{
    public class UnitTest1
    {
        private readonly TestServer _testServer;
        private readonly HttpClient _testClient;
        public UnitTest1()
        {
            //Initializing the test environment
            _testServer = new TestServer(new WebHostBuilder()
                .UseStartup<Startup>());
            //Test client creation
            _testClient = _testServer.CreateClient();
        }

        [Fact]
        public async Task TestGetRequestAsync()
        {
            var responseGet = await _testClient.GetAsync("/api/contacts");
            responseGet.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task TestGetByIdRequestAndResponseOkAsync()
        {
            using var responseGet = await
           _testClient.GetAsync("api/contacts/1");
            // Assert
            responseGet.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task TestGetByIdRequestAndResponseNotOkAsync()
        {
            var responseGet = await _testClient
                .GetAsync("api/contacts/4");
            // Assert
            responseGet.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }

        [Fact]
        public async Task TestGetAdresseByContactIdRequestAndResponseOkAsync()
        {
            var responseGet = await _testClient
                .GetAsync("api/contacts/1/adresses");
            // Assert
            responseGet.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task TestGetAdresseByContactIdRequestAndResponseNotOkAsync()
        {
            var responseGet = await _testClient
                .GetAsync("api/contacts/2/adresses");
            // Assert
            responseGet.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }

        [Fact]
        public async Task TestGetTelephoneByContactIdRequestAndResponseOkAsync()
        {
            var responseGet = await _testClient
                .GetAsync("api/contacts/1/telephones");
            // Assert
            responseGet.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task TestGetTelephoneByContactIdRequestAndResponseNotOkAsync()
        {
            var responseGet = await _testClient
                .GetAsync("api/contacts/2/telephones");
            // Assert
            responseGet.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }

        [Fact]
        public async Task TestPutContactIdRequestAndResponseOkAsync()
        {
            var idContact = 2;
            var contact = new Contacts
            {
                Id = idContact,
                Prenom = "Jacques",
                Nom = "Boulon",
                Adresses = null,
                Telephones = null
            };
            var responsePut = await _testClient
                .PutAsJsonAsync("api/contacts/" + idContact, contact);
            // Assert
            responsePut.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task TestPutContactIdRequestAndResponseIdNotFoundAsync()
        {
            var idContact = 4;
            var contact = new Contacts
            {
                Id = idContact,
                Prenom = "Jacques",
                Nom = "Boulon",
                Adresses = null,
                Telephones = null
            };
            var responsePut = await _testClient
                .PutAsJsonAsync("api/contacts/" + idContact, contact);
            // Assert
            responsePut.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }

        [Fact]
        public async Task TestPutContactIdRequestAndResponseBadRequestAsync()
        {
            var idContact = -1;
            var contact = new Contacts
            {
                Id = idContact,
                Prenom = "Jacques",
                Nom = "Boulon",
                Adresses = null,
                Telephones = null
            };
            var responsePut = await _testClient
                .PutAsJsonAsync("api/contacts/3", contact);
            // Assert
            responsePut.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        [Fact]
        public async Task TestPutAdressToContactRequestAndResponseOkAsync()
        {
            var idContact = 3;
            var idAdresse = 3;

            var adresse = new Adresse
            {
                Id = idAdresse,
                NumeroRue = 48,
                NomRue = "rue de la mairie",
                NomVille = "Saint Saturnin",
                CodePostal = 16290
            };

            var responsePut = await _testClient
                .PutAsJsonAsync("api/contacts/" + idContact + "/adresses/" + idAdresse, adresse);
            // Assert
            responsePut.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task TestPutAdressToContactRequestAndNotFoundContactIdAsync()
        {
            var idContact = 4;
            var idAdresse = 3;

            var adresse = new Adresse
            {
                Id = idAdresse,
                NumeroRue = 48,
                NomRue = "rue de la mairie",
                NomVille = "Saint Saturnin",
                CodePostal = 16290
            };

            var responsePut = await _testClient
                .PutAsJsonAsync("api/contacts/" + idContact + "/adresses/" + idAdresse, adresse);
            // Assert
            responsePut.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }

        [Fact]
        public async Task TestPutAdressToContactRequestAndNotFoundAdressIdAsync()
        {
            var idContact = 3;
            var idAdresse = 7;

            var adresse = new Adresse
            {
                Id = idAdresse,
                NumeroRue = 48,
                NomRue = "rue de la mairie",
                NomVille = "Saint Saturnin",
                CodePostal = 16290
            };

            var responsePut = await _testClient
                .PutAsJsonAsync("api/contacts/" + idContact + "/adresses/" + idAdresse, adresse);
            // Assert
            responsePut.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }

        [Fact]
        public async Task TestPutAdressToContactRequestAndBadRequestAsync()
        {
            var idContact = 3;
            var idAdresse = 3;

            var adresse = new Adresse
            {
                Id = -1,
                NumeroRue = 48,
                NomRue = "rue de la mairie",
                NomVille = "Saint Saturnin",
                CodePostal = 16290
            };

            var responsePut = await _testClient
                .PutAsJsonAsync("api/contacts/" + idContact + "/adresses/" + idAdresse, adresse);
            // Assert
            responsePut.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        [Fact]
        public async Task TestPutTelephoneToContactRequestAndResponseOkAsync()
        {
            var idContact = 3;
            var idTelephone = 3;

            var telephone = new Telephone
            {
                Id = idTelephone,
                NumeroTelephone = 0760743946
            };

            var responsePut = await _testClient
                .PutAsJsonAsync("api/contacts/" + idContact + "/telephones/" + idTelephone, telephone);
            // Assert
            responsePut.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task TestPutTelephoneToContactRequestAndNotFoundContactIdAsync()
        {
            var idContact = 4;
            var idTelephone = 3;

            var telephone = new Telephone
            {
                Id = idTelephone,
                NumeroTelephone = 0760743946
            };

            var responsePut = await _testClient
                .PutAsJsonAsync("api/contacts/" + idContact + "/telephones/" + idTelephone, telephone);
            // Assert
            responsePut.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }

        [Fact]
        public async Task TestPutTelephoneToContactRequestAndNotFoundTelephoneIdAsync()
        {
            var idContact = 3;
            var idTelephone = 7;

            var telephone = new Telephone
            {
                Id = idTelephone,
                NumeroTelephone = 0760743946
            };

            var responsePut = await _testClient
                .PutAsJsonAsync("api/contacts/" + idContact + "/telephones/" + idTelephone, telephone);
            // Assert
            responsePut.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }

        [Fact]
        public async Task TestPutTelephoneToContactRequestAndBadRequestAsync()
        {
            var idContact = 3;
            var idTelephone = 3;

            var telephone = new Telephone
            {
                Id = -1,
                NumeroTelephone = 0760743946
            };

            var responsePut = await _testClient
                .PutAsJsonAsync("api/contacts/" + idContact + "/telephones/" + idTelephone, telephone);
            // Assert
            responsePut.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        [Fact]
        public async Task TestDeleteContactRequestAndResponseOkAsync()
        {
            var idContact = 3;

            var responseDelete = await _testClient
                .DeleteAsync("api/contacts/" + idContact);
            // Assert
            responseDelete.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task TestDeleteContactRequestAndResponseNotFoundAsync()
        {
            var idContact = 4;

            var responseDelete = await _testClient
                .DeleteAsync("api/contacts/" + idContact);
            // Assert
            responseDelete.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }

        [Fact]
        public async Task TestDeleteAdressOfContactRequestAndResponseOkAsync()
        {
            var idContact = 3;
            var idAdresse = 3;

            var responseDelete = await _testClient
                .DeleteAsync("api/contacts/" + idContact + "/adresses/" + idAdresse);
            // Assert
            responseDelete.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task TestDeleteAdressOfContactRequestAndNotFoundContactIdAsync()
        {
            var idContact = 4;
            var idAdresse = 3;

            var responseDelete = await _testClient
                .DeleteAsync("api/contacts/" + idContact + "/adresses/" + idAdresse);
            // Assert
            responseDelete.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }

        [Fact]
        public async Task TestDeleteAdressOfContactRequestAndNotFoundAdressIdAsync()
        {
            var idContact = 3;
            var idAdresse = 4;

            var responseDelete = await _testClient
                .DeleteAsync("api/contacts/" + idContact + "/adresses/" + idAdresse);
            // Assert
            responseDelete.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }

        [Fact]
        public async Task TestDeleteTelephoneOfContactRequestAndResponseOkAsync()
        {
            var idContact = 3;
            var idTelephone = 3;

            var responseDelete = await _testClient
                .DeleteAsync("api/contacts/" + idContact + "/telephone/" + idTelephone);
            // Assert
            responseDelete.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task TestDeleteTelephoneOfContactRequestAndNotFoundContactIdAsync()
        {
            var idContact = 4;
            var idTelephone = 3;

            var responseDelete = await _testClient
                .DeleteAsync("api/contacts/" + idContact + "/telephone/" + idTelephone);
            // Assert
            responseDelete.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }

        [Fact]
        public async Task TestDeleteTelephoneOfContactRequestAndNotFoundAdressIdAsync()
        {
            var idContact = 3;
            var idTelephone = 4;

            var responseDelete = await _testClient
                .DeleteAsync("api/contacts/" + idContact + "/telephone/" + idTelephone);
            // Assert
            responseDelete.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }
    }
}
